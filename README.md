# assignment

1. API First  
  The API design is available in **GitLab pages**.  
  Notes for the theoretical alignment of the FRD is available [here](https://gitlab.com/roncsak/assignment/-/wikis/FRD-alignment).  
  Architecture diagram is available [here](https://gitlab.com/roncsak/assignment/-/wikis/Architecture-diagram).
1. Native vs Crosss-platform mobile - customer letter is available [here](https://gitlab.com/roncsak/assignment/-/wikis/Customer-letter).
1. Security check - PenTest report mitigation plan is available [here](https://gitlab.com/roncsak/assignment/-/wikis/Mitigation-plan).


__General feedback about the assignments and the experience so far:__  
  * I feel a bit mixed about the communication of the time limits
  * With already existing templates for the APIs and Architecture diagram it *might* have been done within 3-4 hours.
  * I enjoyed thinking on the tasks
  * The HR round was easy, like when you talk with your frinds. :thumbsup:
